<%-- 
    Document   : index
    Created on : 25-04-2021, 05:54:25
    Author     : Ricardo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Listado de Productos</h1>
        <form name="form" action="ProductoController" method="POST">
            <fieldset>
                <legend>Información Producto</legend>
                <label>Id <input type="text" name="id"></label>
                <label>Codigo : <input type="text" name="codigo"></label>
                <label>Nombre <input type="text" name="nombre"></label>
                <label>Descripcion : <input type="text" name="descripcion"></label>
            </fieldset>
            <fieldset>
                <legend>Stock</legend>                
                <label>Existencia: <input type="text" name="existencia"></label>
                <label>Precio : <input type="text" name="precio"></label>
            </fieldset>            
                <button type="submit" name="accion" value="ingreso" class="btn btn-success">Ingresar</button>
                <button type="submit" name="accion" value="verlista" class="btn btn-success">Ver Productos</button>
        </form>

    </body>
</html>
