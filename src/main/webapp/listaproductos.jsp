<%-- 
    Document   : listaproductos
    Created on : 26-04-2021, 00:16:45
    Author     : Ricardo
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.registroproductos.entity.RegArticulos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<RegArticulos> productos = (List<RegArticulos>) request.getAttribute("listaproducto");
    Iterator<RegArticulos> iteproductos = productos.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Lista de Productos</h1>
        <form name="form" action="ProductoController" method="POST">
            <table border="1">
                <thead>
                <th>ID</th>
                <th>CODIGO</th>
                <th>NOMBRE</th>
                <th>DESCRIPCION</th>
                <th>STOCK</th>
                <th>PRECIO</th>
                <th>ACCION</th>

                </thead>
                <tbody>
                    <%while (iteproductos.hasNext()) {
                            RegArticulos ra = iteproductos.next();%>
                    <tr>
                        <td><%=ra.getId()%></td>
                        <td><%=ra.getCodigo()%></td>
                        <td><%=ra.getNombre()%></td>
                        <td><%=ra.getDescripcion()%></td>
                        <td><%=ra.getExistencia()%></td>
                        <td><%=ra.getPrecio()%></td>
                        <td><input type="radio" name="seleccion" value="<%=ra.getId()%>"</td>

                    </tr>
                    <%}%>                
                </tbody>

            </table>
                
                <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar</button>
                <button type="submit" name="accion" value="verlista" class="btn btn-success">actualizar lista</button>
                <button type="submit" name="accion" value="editar" class="btn btn-success">Editar</button>
                
        </form>
    </body>
</html>
