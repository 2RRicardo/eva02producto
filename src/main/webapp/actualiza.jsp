<%-- 
    Document   : index
    Created on : 25-04-2021, 05:54:25
    Author     : Ricardo
--%>

<%@page import="com.mycompany.registroproductos.entity.RegArticulos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    RegArticulos producto = (RegArticulos) request.getAttribute("Producto");
    
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Listado de Productos</h1>
        <form name="form" action="ProductoController" method="POST">
            <fieldset>
                <legend>Información Producto</legend>
                <label>Id <input type="text"  name="id"  value="<%=producto.getId()%>"></label>
                <label>Codigo : <input type="text" name="codigo" value="<%=producto.getCodigo()%>"></label>
                <label>Nombre <input type="text" name="nombre" value="<%=producto.getNombre()%>"></label>
                <label>Descripcion : <input type="text" name="descripcion" value="<%=producto.getDescripcion()%>"></label>
            </fieldset>
            <fieldset>
                <legend>Stock</legend>                
                <label>Existencia: <input type="text" name="existencia" value="<%=producto.getExistencia()%>"></label>
                <label>Precio : <input type="text" name="precio" value="<%=producto.getPrecio()%>"></label>
            </fieldset>            
                <button type="submit" name="accion" value="confirmar" class="btn btn-success">Confirmar cambios</button>
                
               <button type="submit" name="accion" value="verlista" class="btn btn-success">actualizar lista</button>
        </form>

    </body>
</html>
