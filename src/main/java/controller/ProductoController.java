/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.registroproductos.dao.RegArticulosJpaController;
import com.mycompany.registroproductos.dao.exceptions.NonexistentEntityException;
import com.mycompany.registroproductos.entity.RegArticulos;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ricardo
 */
@WebServlet(name = "ProductoController", urlPatterns = {"/ProductoController"})
public class ProductoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductoController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductoController at " + request.getContextPath() + "</h1>");
            //out.println("<button onclick=\"http://localhost:8080/registroproductos\">Volver atras</button>");
            out.println("<a href=\"http://localhost:8080/registroproductos\">Volver atras</a>");
            

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");

        if (accion.equals("ingreso")) {

            try {
                String id = request.getParameter("id");
                String codigo = request.getParameter("codigo");
                String nombre = request.getParameter("nombre");
                String descripcion = request.getParameter("descripcion");
                String existencia = request.getParameter("existencia");
                String precio = request.getParameter("precio");

                RegArticulos regarticulos = new RegArticulos();

                regarticulos.setId(id);
                regarticulos.setCodigo(codigo);
                regarticulos.setNombre(nombre);
                regarticulos.setDescripcion(descripcion);
                regarticulos.setExistencia(existencia);
                regarticulos.setPrecio(precio);
                //System.out.println(""+regarticulos.getNombre());
                RegArticulosJpaController dao = new RegArticulosJpaController();
                dao.create(regarticulos);
                
           

            } catch (Exception ex) {
                Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } if (accion.equals("verlista")) {
            System.out.println("Entra al mostra lista");
            RegArticulosJpaController dao = new RegArticulosJpaController();
            List<RegArticulos> lista = dao.findRegArticulosEntities();
            request.setAttribute("listaproducto", lista);
            request.getRequestDispatcher("listaproductos.jsp").forward(request, response);
            
        }if (accion.equals("eliminar")) {
            System.out.println("Entra al eliminar lista");
            try {
                String seleccion = request.getParameter("seleccion");
                RegArticulosJpaController dao = new RegArticulosJpaController();
                dao.destroy(seleccion);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        }if (accion.equals("editar")) {
            System.out.println("Entra al editar lista");
            
            String seleccion = request.getParameter("seleccion");
 
            RegArticulosJpaController dao = new RegArticulosJpaController();
            
            RegArticulos regarticulo = dao.findRegArticulos(seleccion);
            request.setAttribute("Producto", regarticulo);
            request.getRequestDispatcher("actualiza.jsp").forward(request, response);
            
        }
        if (accion.equals("confirmar")){
            
            try{
            
            RegArticulos regarticulos = new RegArticulos();
            
            regarticulos.setId(request.getParameter("id"));
            regarticulos.setCodigo(request.getParameter("codigo"));
            regarticulos.setNombre(request.getParameter("nombre"));
            regarticulos.setDescripcion(request.getParameter("descripcion"));
            regarticulos.setExistencia(request.getParameter("existencia"));
            regarticulos.setPrecio(request.getParameter("precio"));

            RegArticulosJpaController dao = new RegArticulosJpaController();
            dao.edit(regarticulos);
            
            } catch (Exception ex) {
                Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE, null, ex);
            }
                
        
        
        }
            processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
