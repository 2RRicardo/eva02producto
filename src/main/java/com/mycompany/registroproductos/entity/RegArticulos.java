/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.registroproductos.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ricardo
 */
@Entity
@Table(name = "reg_articulos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegArticulos.findAll", query = "SELECT r FROM RegArticulos r"),
    @NamedQuery(name = "RegArticulos.findById", query = "SELECT r FROM RegArticulos r WHERE r.id = :id"),
    @NamedQuery(name = "RegArticulos.findByCodigo", query = "SELECT r FROM RegArticulos r WHERE r.codigo = :codigo"),
    @NamedQuery(name = "RegArticulos.findByNombre", query = "SELECT r FROM RegArticulos r WHERE r.nombre = :nombre"),
    @NamedQuery(name = "RegArticulos.findByDescripcion", query = "SELECT r FROM RegArticulos r WHERE r.descripcion = :descripcion"),
    @NamedQuery(name = "RegArticulos.findByExistencia", query = "SELECT r FROM RegArticulos r WHERE r.existencia = :existencia"),
    @NamedQuery(name = "RegArticulos.findByPrecio", query = "SELECT r FROM RegArticulos r WHERE r.precio = :precio")})
public class RegArticulos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "id")
    private String id;
    @Size(max = 2147483647)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 2147483647)
    @Column(name = "existencia")
    private String existencia;
    @Size(max = 2147483647)
    @Column(name = "precio")
    private String precio;

    public RegArticulos() {
    }

    public RegArticulos(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getExistencia() {
        return existencia;
    }

    public void setExistencia(String existencia) {
        this.existencia = existencia;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegArticulos)) {
            return false;
        }
        RegArticulos other = (RegArticulos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.registroproductos.entity.RegArticulos[ id=" + id + " ]";
    }
    
}
