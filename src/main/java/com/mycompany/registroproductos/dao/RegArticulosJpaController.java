/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.registroproductos.dao;

import com.mycompany.registroproductos.dao.exceptions.NonexistentEntityException;
import com.mycompany.registroproductos.dao.exceptions.PreexistingEntityException;
import com.mycompany.registroproductos.entity.RegArticulos;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Ricardo
 */
public class RegArticulosJpaController implements Serializable {

    public RegArticulosJpaController() {
        
    }
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(RegArticulos regArticulos) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(regArticulos);
            em.getTransaction().commit();
            System.out.println("Creando registro");
        } catch (Exception ex) {
            if (findRegArticulos(regArticulos.getId()) != null) {
                throw new PreexistingEntityException("RegArticulos " + regArticulos + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(RegArticulos regArticulos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            regArticulos = em.merge(regArticulos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = regArticulos.getId();
                if (findRegArticulos(id) == null) {
                    throw new NonexistentEntityException("The regArticulos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RegArticulos regArticulos;
            try {
                regArticulos = em.getReference(RegArticulos.class, id);
                regArticulos.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The regArticulos with id " + id + " no longer exists.", enfe);
            }
            em.remove(regArticulos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<RegArticulos> findRegArticulosEntities() {
        return findRegArticulosEntities(true, -1, -1);
    }

    public List<RegArticulos> findRegArticulosEntities(int maxResults, int firstResult) {
        return findRegArticulosEntities(false, maxResults, firstResult);
    }

    private List<RegArticulos> findRegArticulosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RegArticulos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public RegArticulos findRegArticulos(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(RegArticulos.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegArticulosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RegArticulos> rt = cq.from(RegArticulos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
